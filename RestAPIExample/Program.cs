using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RestAPIExample.Data;
using RestAPIExample.Models;

namespace RestAPIExample
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();

            //using (var context = new CategoriesContext())
            //{
            //    var categoryOne = new Models.Categories
            //    {
            //        Id = 1,
            //        Category = "Simple"
            //    };

            //    var categoryTwo = new Models.Categories
            //    {
            //        Id = 2,
            //        Category = "Macabre"
            //    };

            //    context.Categories.Add(categoryOne);
            //    context.Categories.Add(categoryTwo);
            //    context.SaveChanges();
            //}

            //using (var context = new JokesContext())
            //{
            //    var jokeOne = new Jokes
            //    {
            //        Id = 1,
            //        Name = "Switzerland",
            //        Joke = "What's the best thing about Switzerland? Answer: I don't know, but the flag is a big plus",
            //        CategoriesId = Categories(1)
            //    };

            //    context.Jokes.Add(jokeOne);
            //}

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
