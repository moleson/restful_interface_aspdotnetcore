﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestAPIExample.Models;
using RestAPIExample.Data;

namespace RestAPIExample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JokesController : ControllerBase
    {
        private readonly JokesContext _context;

        public JokesController(JokesContext context)
        {
            _context = context;
        }

        // GET: api/Jokes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Jokes>>> GetJokes()
        {
            return await _context.Jokes.ToListAsync();
        }

        // GET: api/Jokes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Jokes>> GetJokes(long id)
        {
            var jokes = await _context.Jokes.FindAsync(id);

            if (jokes == null)
            {
                return NotFound();
            }

            return jokes;
        }

        // PUT: api/Jokes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutJokes(long id, Jokes jokes)
        {
            if (id != jokes.Id)
            {
                return BadRequest();
            }

            _context.Entry(jokes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JokesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Jokes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Jokes>> PostJokes(Jokes jokes)
        {
            _context.Jokes.Add(jokes);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetJokes", new { id = jokes.Id }, jokes);
        }

        // DELETE: api/Jokes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Jokes>> DeleteJokes(long id)
        {
            var jokes = await _context.Jokes.FindAsync(id);
            if (jokes == null)
            {
                return NotFound();
            }

            _context.Jokes.Remove(jokes);
            await _context.SaveChangesAsync();

            return jokes;
        }

        private bool JokesExists(long id)
        {
            return _context.Jokes.Any(e => e.Id == id);
        }
    }
}
