﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIExample.Models
{
    public class Jokes
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Joke { get; set; }
        public Categories Category { get; set; }
    }
}
