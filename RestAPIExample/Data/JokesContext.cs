﻿using Microsoft.EntityFrameworkCore;
using RestAPIExample.Models;

namespace RestAPIExample.Data
{
    public class JokesContext : DbContext
    {
        public JokesContext(DbContextOptions<JokesContext> options) : base(options)
        {

        }

        public JokesContext()
        {

        }

        public DbSet<Jokes> Jokes { get; set; }
        public DbSet<Categories> Categories { get; set; }
    }
}
